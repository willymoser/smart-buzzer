package ufrn.dca.smartbuzzer.cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Willy, Tiago e Fabio
 * 
 */

public class Cliente {
    
    private Socket socket;
    private String ip = "127.0.0.1";
    private int porta = 2424;

    public Cliente(String ip) {
        this.ip = ip;
    }

    public Cliente(String ip, int porta) {
        this.porta = porta;
        this.ip = ip;
    }
    
    public void Conectar() {
        try {
            this.socket = new Socket(ip, porta);
        } catch (IOException erro) {
            System.err.println("Erro Class Cliente - " + erro);
        }
    }
    
    public void enviarPacote(String pacote) {
        try {
            socket.getOutputStream().write(pacote.getBytes());
        } catch (IOException erro) {
            System.err.println("Erro Class Cliente - " + erro);
        }
    }
    
    public String receberPacote() {
        String pacote = "";
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            pacote = entrada.readLine();
            return pacote;
        } catch (IOException erro) {
            System.err.println("Erro Class Cliente. - " + erro);
        }
        return pacote;
    }
    
    public void fecharConexao() {
        try {
            socket.close();
        } catch (IOException erro) {
            System.err.println("Erro Class Cliente - " + erro);
        }
                
    }
    
    
}
