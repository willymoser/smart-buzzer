package ufrn.dca.smartbuzzer.servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author Willy, Tiago e Fabio
 * 
 */

public class Gerenciador implements Runnable {
    
    private int tipo;
    private int destino;
    private String dados = null;
    private String pacote = null;
    private String[] pacote_temp = null;
    private BufferedReader entrada = null;
    private final Servidor servidor;
    private final Socket cliente;
    
    public Gerenciador(Servidor servidor, Socket cliente) {
        this.servidor = servidor;
        this.cliente = cliente;
    }

    @Override
    public void run() {
        
        try {            
            
            entrada = new BufferedReader(new InputStreamReader(cliente.getInputStream()));            
            
            while(true) {
                
                pacote = entrada.readLine();
                pacote_temp = pacote.split("#");
                tipo = Integer.parseInt(pacote_temp[2]);
                
                if (tipo == Servidor.FECHAR_CONEXAO) {
                    cliente.close();
                    servidor.removerCliente(tipo);
                    break;
                }
                
                destino = Integer.parseInt(pacote_temp[1]);
                dados = pacote_temp[3];
                pacote = tipo + "#" + dados + "#\n";
                servidor.encaminharPacote(destino, pacote);
                pacote_temp = null;
                
            }
            
        } catch (IOException erro) {
            System.err.println("Erro na thread - " + erro);
        }

    }
    
}
