package ufrn.dca.smartbuzzer.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 *
 * @author Willy, Tiago e Fabio
 * 
 */
 
public class Servidor {
    
    public static final int TIPO_CONEXAO   = 0;
    public static final int TIPO_IMAGEM    = 1;
    public static final int TIPO_AUDIO     = 2;
    public static final int FECHAR_CONEXAO = 3;
    
    private int porta = 2424;
    private ServerSocket servidor;
    private final HashMap<Integer, Socket> listaclientes;

    public Servidor() {
        this.listaclientes = new HashMap();
    }
    
    public Servidor(int porta) {
        this.listaclientes = new HashMap();
        this.porta = porta;
    }
    
    public void iniciarServidor() {
        try {
            servidor = new ServerSocket(porta);
        } catch (IOException erro) {
            System.err.println("Servidor: iniciarServidor() - " + erro);
        }
    }
    
    public boolean encaminharPacote(int destino, String dados) {
        try {
            listaclientes.get(destino).getOutputStream().write(dados.getBytes());
            return true;
        } catch (IOException erro) {
            System.err.println("Servidor: enviarPacote() - " + erro);
            return false;
        }
    }
    
    public void fecharServidor() {
        try {
            servidor.close();
        } catch (IOException erro) {
            System.out.println("Servidor: fecharServidor - " + erro);
        }
    }
    
    public void adicionarCliente(int id, Socket cliente) {
        listaclientes.put(id, cliente);
    }
    
    public void removerCliente(int id) {
        listaclientes.remove(id);
    }

    public ServerSocket getServidor() {
        return servidor;
    }

    public int getPorta() {
        return porta;
    }
    
}