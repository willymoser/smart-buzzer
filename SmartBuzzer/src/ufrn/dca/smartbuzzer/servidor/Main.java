package ufrn.dca.smartbuzzer.servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author Wyle
 * 
 */

public class Main {
    
    public static void main(String[] args) {
        
        Socket cliente;
        Servidor serv = new Servidor(2424);
        serv.iniciarServidor();
        BufferedReader entrada = null;
        
        while(true) {
            
            try {
                
                System.out.println("> Esperando por cliente");
                cliente = serv.getServidor().accept();
                System.out.println(">> Novo cliente conectado");
                System.out.println(">>> Esperando pacote de confirmação ...");
                entrada = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
                String pacote = entrada.readLine();
                System.out.println(">>>> Pacote recebido: " + pacote);
                Gerenciador gerenciador = new Gerenciador(serv, cliente);
                Thread t = new Thread(gerenciador);
                
                String[] pacote_temp = pacote.split("#");
                for (String pacote_temp1 : pacote_temp) {
                    System.out.println("->" + pacote_temp1);
                }
                if (Integer.parseInt(pacote_temp[2]) == Servidor.TIPO_CONEXAO) {
                    int origem = Integer.parseInt(pacote_temp[0]);
                    serv.adicionarCliente(origem, cliente);
                    System.out.println(">>>>> Cliente adicionado ao Servidor");
                    t.start();
                }else{
                    gerenciador = null;
                    t = null;
                    System.out.println(">>>>> Cliente não adicionado adicionado ao Servidor");
                    cliente.close();
                }
                
            } catch (IOException erro) {                
                System.err.println("Erro na Main do servidor: " + erro);
                serv.fecharServidor();
            }
            
        }

    }
    
}
